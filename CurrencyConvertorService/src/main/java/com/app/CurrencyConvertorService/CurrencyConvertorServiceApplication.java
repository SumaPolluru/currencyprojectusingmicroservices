package com.app.CurrencyConvertorService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class CurrencyConvertorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyConvertorServiceApplication.class, args);
	}
	@Bean
	public RestTemplate getTemplate()
	{
		return new RestTemplate();
	}

}
