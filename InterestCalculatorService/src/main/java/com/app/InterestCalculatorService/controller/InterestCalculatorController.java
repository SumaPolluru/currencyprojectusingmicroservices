package com.app.InterestCalculatorService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class InterestCalculatorController {
	@Autowired
	RestTemplate template;
    HttpHeaders headers = new HttpHeaders();
    HttpEntity <String> entity = new HttpEntity<String>(headers);
    
	@GetMapping("/calculateInterst/{dataamount}")
	public Double calculateInterest(@PathVariable String dataamount)
	{
	Double amount=Double.parseDouble(dataamount);	
		System.out.println("Amount coming here is "+amount);
		double interest=0.0;
		if(amount>20000&&amount<30000)
		{
			interest=(amount*0.03);
		}
		else if(amount>30000&&amount<60000)
		{
			interest=amount*0.05;
			
		}
		else
		{
			interest=amount*0.07;
		}
		return interest;
	}
	
	@GetMapping("/convertorService/{amount}")
	public Double convertToRs(@PathVariable("amount") Double amt)
	{
		System.out.println("Amount is coming as"+amt);
		 return template.exchange("http://localhost:8081/converttors/"+amt,
				 HttpMethod.GET,entity, Double.class).getBody();

	}
	

}
