package com.app.CurrencyConvertorService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CurrencyController {
	@Autowired
	RestTemplate template;
    HttpHeaders headers = new HttpHeaders();
    HttpEntity <String> entity = new HttpEntity<String>(headers);
    
	@GetMapping("/converttors/{dataamount}")
	public Double getToRs(@PathVariable String dataamount)
	{
		Double amount=Double.parseDouble(dataamount);	
		System.out.println("Amount coming here is "+amount);
		Double convertedamount=(amount*85);
		return  convertedamount;
	}
	
	@GetMapping("/calculatorService/{amount}")
	public Double calculateInterest(@PathVariable("amount") Double amt)
	{
		System.out.println("Amount is coming as"+amt);
		 return template.exchange("http://localhost:8082/calculateInterst/"+amt,
				 HttpMethod.GET,entity, Double.class).getBody();

	}
}

