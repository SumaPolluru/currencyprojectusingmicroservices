package com.app.CurrencyConsumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HelloController {
	@Autowired
	RestTemplate template;
    HttpHeaders headers = new HttpHeaders();
  
    HttpEntity <String> entity = new HttpEntity<String>(headers);

	@RequestMapping("/welcome")
	public ModelAndView welcomepage()
	{
		System.out.println("Coming here ");
		return new ModelAndView("index");
	}
	@GetMapping("/calculatorService/{amount}")
	public Double calculateInterest(@PathVariable("amount") Double amt)
	{
		System.out.println("Amount is coming as"+amt);
		 return template.exchange("http://localhost:8082/calculateInterst/"+amt,
				 HttpMethod.GET,entity, Double.class).getBody();

	}
	@GetMapping("/convertorService/{amount}")
	public Double convertToRs(@PathVariable("amount") Double amt)
	{
		System.out.println("Amount is coming as"+amt);
		 return template.exchange("http://localhost:8081/converttors/"+amt,
				 HttpMethod.GET,entity, Double.class).getBody();

	}
	

}

