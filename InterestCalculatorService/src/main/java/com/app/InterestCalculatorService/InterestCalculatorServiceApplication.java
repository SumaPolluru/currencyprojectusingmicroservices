package com.app.InterestCalculatorService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class InterestCalculatorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterestCalculatorServiceApplication.class, args);
	}
	@Bean
	public RestTemplate getTemplate()
	{
		return new RestTemplate();
	}

}
